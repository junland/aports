# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=xdg-desktop-portal-kde
pkgver=5.27.0
pkgrel=0
pkgdesc="A backend implementation for xdg-desktop-portal that is using Qt/KDE"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://phabricator.kde.org/source/xdg-desktop-portal-kde"
license="LGPL-2.0-or-later"
depends="xdg-desktop-portal"
makedepends="
	cups-dev
	extra-cmake-modules
	glib-dev
	kcoreaddons-dev
	kdeclarative-dev
	kio-dev
	kirigami2-dev
	kwayland-dev
	libepoxy-dev
	pipewire-dev
	plasma-framework-dev
	plasma-wayland-protocols
	qt5-qtbase-dev
	samurai
	"
subpackages="$pkgname-lang"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/xdg-desktop-portal-kde-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKDE_INSTALL_LIBEXECDIR=libexec
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	rm -rf "$pkgdir"/usr/lib/systemd
}

sha512sums="
231f7168eaea5726289193bd84a4ce9cfbdcc13f230f9ab3e5030687277963746ad0fdb9af7f8a54b4d06ccaea675d8bcab9fdfdce509d64cce853c45d9cb383  xdg-desktop-portal-kde-5.27.0.tar.xz
"
