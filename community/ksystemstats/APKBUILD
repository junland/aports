# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ksystemstats
pkgver=5.27.0
pkgrel=0
pkgdesc="A plugin based system monitoring daemon"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="LicenseRef-KDE-Accepted-GPL AND LicenseRef-KDE-Accepted-LGPL AND CC0-1.0"
makedepends="
	eudev-dev
	extra-cmake-modules
	kcoreaddons-dev
	kdbusaddons-dev
	kio-dev
	libksysguard-dev
	libnl3-dev
	lm-sensors-dev
	networkmanager-qt-dev
	qt5-qtbase-dev
	samurai
	solid-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/ksystemstats-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Broken, requires specific sensor setup

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
407d4eac266a293cd59582065a126ddd7897b912dba3dd5f65e56e6a7145f606b8c850fc5e3275af13f2d61381bcaf8fff48f24c49928b451f5671c2856a95d3  ksystemstats-5.27.0.tar.xz
"
